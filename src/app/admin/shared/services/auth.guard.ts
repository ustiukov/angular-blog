import {inject} from "@angular/core";
import {CanActivateFn, Router} from "@angular/router";
import {Observable} from "rxjs";
import {AuthService} from "./auth.service";

export const AuthGuard: CanActivateFn = (): Observable<boolean> | Promise<boolean> | boolean => {
  const router = inject(Router)
  const auth = inject(AuthService)

  if (auth.isAuthenticated()) return true
  else {
    auth.logout()
    router.navigate(['/admin', 'login'], {
      queryParams: {
        loginAgain: true
      }
    })
    return false
  }
}
